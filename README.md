# Skate App

The app for skaters.

## Versions

- Java 11
- Node v14.15.4

## Commands

### Production:

1. Make sure to have Docker installed
2. Clone the skate-app and skate-ui repositories into the same directory (E.g. ```projects/skate-app``` & ```projects/skate-ui```)
3. Run the following chained commands in ```projects/skate-app```:

```console
docker build -t skate-app . && docker build -t skate-ui ../skate-ui && docker-compose up -d
```

4. Visit ```localhost:3000``` in a web browser

Note: Change passwords etc. if doing an actual deployment.

### Development:

Commands to use in terminal while in skate-app project folder.

First install docker.

Start the all parts of the application in docker containers:
0. One-liner for building or updating both images:

```console
mvn package && docker build -t skate-app . && docker build -t skate-ui ../skate-ui && docker-compose up -d --build skate-app skate-ui
```

1. build images:

```console
mvn package
docker build -t skate-app .
docker build -t skate-ui ../skate-ui
```

2. run the compose file:

```console
docker-compose up -d
```

Update the images by:

1. build new images:

```console
mvn package
docker build -t skate-app .
docker build -t skate-ui ../skate-ui
```

2. update the running containers with the new images:

```console
docker-compose up -d --build skate-app skate-ui
```    

Login to MySQL database:

Passwords:
root =>```root```
springuser => ```password```

1. Enter the container

```console
docker exec -it skate-app_database_1 bash
```

2. Login to MySQL in container

```console
mysql -u root -p
```

3. Enter the skatedb database

```console
use skatedb;
```

## Background

School project.

## Vision

_Helping skaters create their community._

## Stakeholders

Skaters.

## Basic Conditions

Deadline V1.0: **21.05.2021**

## Requirements

### Functional

### Non-Functional

## Architecture

### High Level

## Technology

### Backend

### Platform and Programming Language

Java

#### Frameworks

Spring Boot

### Frontend

JavaScript, HTML and CSS.

#### Frameworks

React

### Database

MongoDB?

### Deployment

#### Hosting

## MVPs (Epics)

| ID | Name | Repositories | Status |
| --- | --- | --- | --- |
| 1 | example | to come | Not started |

## MVP Backlog

1. Skate Spot Map Service
2. Account Service
3. Skate Spot Metadata Service
4. Menu (only frontend?)
5. Profile Service
6. Feed Service
7. Event Service
8. Notification Service
9. Map Search Service
10. Friends Service
11. Skatetrick Wiki Service
