package com.example.skateapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The entry point into the spring boot application
 */
@SpringBootApplication
public class Main {

    /**
     * Main method of the application that runs the spring application
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
