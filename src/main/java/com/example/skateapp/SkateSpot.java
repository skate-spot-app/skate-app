package com.example.skateapp;

import javax.persistence.*;

/**
 * Represents a skate spot
 *
 * Uses @Entity to create a table for SkateSpot's in the MySQL database
 */
@Entity
public class SkateSpot {

    /**
     * Auto generated id for the skate spot
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * Title of the skate spot
     */
    private String title;

    /**
     * Latitude coordinate of the skate spot
     */
    private double lat;

    /**
     * Longitude coordinate of the skate spot
     */
    private double lng;

    /**
     * Create the date for the skate spot
     */
    private String createDate;

    /**
     * Description of the spot
     */
    private String description;

    /**
     * Opening time of the skate spot
     */
    private String openingTimes;

    /**
     * address of the skate spot
     */
    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOpeningTimes() {
        return openingTimes;
    }

    public void setOpeningTimes(String openingTimes) {
        this.openingTimes = openingTimes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
