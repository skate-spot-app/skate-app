package com.example.skateapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The main controller for handling requests for the skate app
 */
@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/")
public class MainController {

    @Autowired
    private SkateSpotRepository skateSpotRepository;

    /**
     * Mapping for adding a new skate spot
     *
     * @param title - Title of the skate spot
     * @param lat - Latitude coordinate of the skate spot
     * @param lng - Longitude coordinate of the skate spot
     * @param description - Describes the skate spot
     * @param openingTimes - When the skate spot is open for skaters
     * @param address - The location of the skate spot
     * @return String - Success message
     */
    @PostMapping(path = "/add")
    public @ResponseBody String addNewSkateSpot(@RequestParam String title, double lat, double lng, String description,
                                                String openingTimes, String address) {
        SkateSpot skateSpot = new SkateSpot();
        skateSpot.setTitle(title);
        skateSpot.setLat(lat);
        skateSpot.setLng(lng);
        skateSpot.setCreateDate(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
        skateSpot.setDescription(description);
        skateSpot.setOpeningTimes(openingTimes);
        skateSpot.setAddress(address);

        skateSpotRepository.save(skateSpot);
        return "Saved"; // TODO: send error if no success
    }

    /**
     * Mapping for getting all the skate spots in the database
     *
     * @return Iterable<SkateSpot> - List of skate spots
     */
    @GetMapping(path = "/all")
    public @ResponseBody Iterable<SkateSpot> getAllSkateSpots() {
        return skateSpotRepository.findAll();
    }
}
