package com.example.skateapp;

import org.springframework.data.repository.CrudRepository;

/**
 * SkateSpotRepository extends the CrudRepository to use existing CRUD operations
 */
public interface SkateSpotRepository extends CrudRepository<SkateSpot, Integer> {
}
