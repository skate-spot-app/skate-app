FROM openjdk:11
ADD "target/skate-app-0.0.1-SNAPSHOT.jar" "skate-app.jar"
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "skate-app.jar"]